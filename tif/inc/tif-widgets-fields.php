<?php

if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * File that holds helper functions that display widget fields in the dashboard
 *
 *  Widget Pack
 * @version 1.0
 */

/**
 * Widget form fields helper function
 *
 *
 * @param	object	$instance		Widget instance
 * @param	array	$widget_field	Widget field array
 * @param	string	$field_value	Field value
 *
 * @since	tif Widget Pack 1.0
 */
function tif_widget_show_fields( $instance = '', $widget_field = '', $tif_field_value = '' ) {

	extract( $widget_field );

	if ( ! isset( $tif_widget_type) )
		return;

	switch( $tif_widget_type ) {

		// Standard text field
		case 'text' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label>
				<input class="" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="text" value="<?php echo esc_html( $tif_field_value ); ?>" />

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		// Textarea field
		case 'textarea' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label>
				<textarea class="" rows="6" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_field_value ); ?></textarea>
			</p>
			<?php
		break;

		// Checkbox field
		case 'checkbox' : ?>
			<p>
				<input id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="checkbox" value="1" <?php checked( '1', $tif_field_value ); ?>/>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?></label>

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		// Radio fields
		case 'radio' : ?>
			<p>
				<?php
				echo esc_html( $tif_widget_title );
				echo '<br />';
				foreach ( $tif_widget_options as $tif_option_name => $tif_option_title ) { ?>
					<input id="<?php echo esc_attr( $instance->get_field_id( $tif_option_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="radio" value="<?php echo esc_attr( $tif_option_name ); ?>" <?php checked( $tif_option_name, $tif_field_value ); ?> />
					<label for="<?php echo esc_attr( $instance->get_field_id( $tif_option_name ) ); ?>"><?php echo esc_html( $tif_option_title ); ?></label>
					<br />
				<?php } ?>

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		// Select field
		case 'select' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label>
				<select name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" class="">
					<?php
					foreach ( $tif_widget_options as $tif_option_name => $tif_option_title ) { ?>
						<option value="<?php echo esc_attr( $tif_option_name ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tif_option_name ) ); ?>" <?php selected( $tif_option_name, $tif_field_value ); ?>><?php echo esc_html( $tif_option_title ); ?></option>
					<?php } ?>
				</select>

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		case 'number' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label><br />
				<input name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="number" step="1" min="0" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" value="<?php echo esc_html( $tif_field_value ); ?>" class="small-text" />

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		// Standard date field
		case 'date' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label>
				<input class="custom_date" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="date" value="<?php echo esc_html( $tif_field_value ); ?>" />
				<script type="text/javascript">
				// jQuery(document).ready(function($) {
				// $('.custom_date').datepicker({
				// dateFormat : 'dd-mm-yy'
				// });
				// });
				</script>
				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		case 'email' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?>:</label>
				<input class="" id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" type="email" value="<?php echo esc_html( $tif_field_value ); ?>" />

				<?php if ( isset( $tif_widget_description ) ) { ?>
				<br />
				<small><?php echo esc_html( $tif_widget_description ); ?></small>
				<?php } ?>
			</p>
			<?php
		break;

		case 'color' : ?>
			<p>
				<label for="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>"><?php echo esc_html( $tif_widget_title ); ?></label>
				<br />
					<input id="<?php echo esc_attr( $instance->get_field_id( $tif_widget_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tif_widget_name ) ); ?>" value="<?php echo esc_html( $tif_field_value ); ?>" class="color"/>
			</p>
			<?php
		break;



	}

}

function tif_widget_updated_field_value( $widget_field, $new_field_value ) {

	 /**
	 * Adds custom widget
	 *
	 * Helper function that updates fields in the dashboard form
	 *
	 *  Widget Pack
	 * @version 1.0
	 */

	extract( $widget_field );

	// Allow some tags in textareas
	if ( $tif_widget_type == 'number' ) {
		return absint( $new_field_value );

	// Allow some tags in textareas
	} elseif ( $tif_widget_type == 'textarea' ) {
		// Check if field array specifed allowed tags
		if ( !isset( $tif_widget_allowed_tags ) ) {
			// If not, fallback to default tags
			$tif_widget_allowed_tags = '<p><strong><em><a>';
		}
		return strip_tags( $new_field_value, $tif_widget_allowed_tags );

	// No allowed tags for all other fields
	} else {
		return strip_tags( $new_field_value );
	}

}
