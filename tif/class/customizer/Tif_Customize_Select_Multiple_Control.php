<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_select_multiple_control' ) ) {

	add_action( 'customize_register', 'tif_extend_select_multiple_control' );

	function tif_extend_select_multiple_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Select_Multiple_Control extends WP_Customize_Control {

			/**
			 * Multiple checkbox customize control class.
			 *
			 * @since  1.0.0
			 * @access public
			 */

			public $type = 'tif-select-multiple';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				$values    = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
				$alignment = isset ( $this->input_attrs['alignment'] ) ? $this->input_attrs['alignment'] : 'row' ;

				?>

				<ul class="tif-multiselect <?php echo esc_attr( $alignment ) ?>">

					<?php

					$i = 0;
					foreach ( $this->choices as $key => $value ) {

					?>

						<li>

							<label>

								<?php

								if ( isset ( $value['label'] ) && $value['label'] )
									echo esc_html( $value['label'] );

								?>

								<select name="<?php esc_attr( $key ) ?>">

									<?php

									foreach ( $value['choices'] as $k => $v ) {

										echo '<option value="' . tif_sanitize_key( $k ) . '"' .
											( selected( $k, $values[$i] ) ) . '>' .
											esc_html( $v ) .
											'</option>';

									}

									?>
								</select>

							</label>

						</li>

					<?php

					++$i;
					}

					?>

					<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />

				</ul>

			<?php

			}
		}

	}

}
