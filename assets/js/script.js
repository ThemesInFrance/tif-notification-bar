//
// Notification Bar js
//

function tifSetCookie(cookieName, cookieValue, nDays, elementToHide) {
    var today = new Date();
    var expire = new Date();
    if (nDays==null || nDays==0) nDays=1;
    expire.setTime(today.getTime() + 3600000*24*nDays);
    document.cookie = cookieName+"="+escape(cookieValue)+ ";expires="+expire.toUTCString()+"; path=/";

    if (elementToHide) {
        jQuery(elementToHide).hide();
    }

}
function tifReadCookie(cookieName) {
    var theCookie=" "+document.cookie;
    var ind=theCookie.indexOf(" "+cookieName+"=");
    if (ind==-1) ind=theCookie.indexOf(";"+cookieName+"=");
    if (ind==-1 || cookieName=="") return "";
    var ind1=theCookie.indexOf(";",ind+1);
    if (ind1==-1) ind1=theCookie.length;
    return unescape(theCookie.substring(ind+cookieName.length+2,ind1));
}
function tifDeleteCookie(cookieName) {
    var today = new Date();
    var expire = new Date() - 30;
    expire.setTime(today.getTime() - 3600000*24*90);
    document.cookie = cookieName+"="+escape(cookieValue)+ ";expires="+expire.toUTCString();
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

(function (window, document) {
    "use strict";

    var onAnnouncementPeriode = function (messages, closeButton) {
        var body = document.getElementsByTagName('body')[0];

        // Create the main black full screen inner
        var screen = document.createElement('div');
        screen.setAttribute('id', 'tif-notification-bar');
        screen.setAttribute('class', 'tif-notification-bar ' + tifMessageInit.init.layout);
        body.appendChild(screen);

        var inner = document.createElement('div');
		inner.setAttribute('class', 'inner ' + tifMessageInit.init.boxed);
        screen.appendChild(inner);

        var container = document.createElement('div');
        inner.appendChild(container);

        // Create a `p` element for each phrase.
        messages.forEach(function (m) {
            var p = document.createElement('p');
            p.insertAdjacentHTML('beforeend', m);
            container.appendChild(p);
        });

        if (closeButton) {
			inner.insertAdjacentHTML('beforeend',closeButton);
		}
    };

    window.tifMessage = function (dateStart, dateEnd, messages, closeButton) {

        var today =  Date.parse(formatDate(new Date()));
        var beginDate = Date.parse(dateStart);
        var endDate = Date.parse(dateEnd);

        if ( today >= beginDate && today <= endDate ) {
            onAnnouncementPeriode(messages, closeButton);
        }
    };

})(window, document);
