<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_notification_bar_options_page() {

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-notification-bar' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php _e( 'Message', 'tif-notification-bar' ) ?></h1>
		<p><?php _e( 'Simply display a message to your users.', 'tif-notification-bar' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-notification-bar' );
			// do_settings_sections( 'tif-notification-bar' );
			wp_nonce_field( 'tif_notification_bar_action', 'tif_notification_bar_nonce_field' );
			$tif_plugin_name = 'tif_plugin_notification_bar';

			// Create a new instance
			// Pass in a URL to set the action
			$form = new Tif_Form_Builder();

			// Form attributes are modified with the set_att function.
			// First argument is the setting
			// Second argument is the value
			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			// $form->set_att( 'id', 'megapouet' );
			$form->set_att( 'novalidate', false );
			// $form->set_att( 'action', 'options.php' );
			// $form->set_att( 'add_honeypot', false );
			// $form->set_att( 'add_nonce', 'tif_notification_bar' );
			$form->set_att( 'is_array', 'tif_plugin_notification_bar' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			// Uss add_input to create form fields
			// First argument is the name
			// Second argument is an array of arguments for the field
			// Third argument is an alternative name field, if needed

			/**
			 * New tab for content
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Content', 'tif-notification-bar' ),
				'dashicon' => 'dashicons-welcome-write-blog',
				'first' => true,
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Content', 'tif-notification-bar' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-content.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			// /**
			//  * New tab for options
			//  */
			// $form->add_input( 'tabs' . $tabs++, array(
			// 	'type' => 'tabs',
			// 	'id' => 'tab-' . $tabs,
			// 	'value' => $tabs,
			// 	'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
			// 	'label' => esc_html__( 'Options', 'tif-notification-bar' ),
			// 	// 'dashicon' => 'dashicons-admin-appearance',
			// 	'dashicon' => 'dashicons-screenoptions',
			// 	'wrap_tag' => '',
			// 	'before_html' => '</div>',
			// ) );
			//
			// $form->add_input( 'html' . $count++ , array(
			// 	'type' => 'html',
			// 	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Options legend', 'tif-notification-bar' ) . '</legend>'
			// ) );
			//
			// 	require_once 'tif-settings-tab-options.php';
			//
			// $form->add_input( 'html' . $count++, array(
			// 	'type' => 'html',
			// 	'value' => '</fieldset>'
			// ) );

			/**
			 * New tab for layout settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Layout', 'tif-notification-bar' ),
				'dashicon' => 'dashicons-admin-appearance',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Layout', 'tif-notification-bar' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-layout.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Settings', 'tif-notification-bar' ),
				'dashicon' => 'dashicons-admin-settings',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Settings', 'tif-notification-bar' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-settings.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			// /**
			//  * New tab to display Shortcode
			//  */
			// $form->add_input( 'tabs' . $tabs++, array(
			// 	'type' => 'tabs',
			// 	'id' => 'tab-' . $tabs,
			// 	'value' => $tabs,
			// 	'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
			// 	'label' => esc_html__( 'Shortcode', 'tif-notification-bar' ),
			// 	'dashicon' => 'dashicons-editor-code',
			// 	'wrap_tag' => '',
			// 	'before_html' => '</div>',
			// ) );
			//
			// $form->add_input( 'html' . $count++ , array(
			// 	'type' => 'html',
			// 	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Shortcode legend', 'tif-notification-bar' ) . '</legend>'
			// ) );
			//
			// 	require_once 'tif-settings-tab-shortcode.php';
			//
			// $form->add_input( 'html' . $count++, array(
			// 	'type' => 'html',
			// 	'value' => '</fieldset>'
			// ) );
			//
			// /**
			//  * New tab for Preview result
			//  */
			// $form->add_input( 'tabs' . $tabs++, array(
			// 	'type' => 'tabs',
			// 	'id' => 'tab-' . $tabs,
			// 	'value' => $tabs,
			// 	'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
			// 	'label' => esc_html__( 'Preview', 'tif-notification-bar' ),
			// 	'dashicon' => 'dashicons-visibility',
			// 	'wrap_tag' => '',
			// 	'before_html' => '</div>',
			// ) );
			//
			// $form->add_input( 'html' . $count++ , array(
			// 	'type' => 'html',
			// 	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Preview legend', 'tif-notification-bar' ) . '</legend>'
			// ) );
			//
			// 	require_once 'tif-settings-tab-preview.php';
			//
			// $form->add_input( 'html' . $count++, array(
			// 	'type' => 'html',
			// 	'value' => '</fieldset>'
			// ) );

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-notification-bar' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<div class="tif-help"><fieldset>'."\n".'<legend>' . esc_html__( 'Help', 'tif-notification-bar' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-help.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset></div>'
			) );

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
