<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Plugin Enabled', 'tif-notification-bar' ),
	array(
		'type'            => 'checkbox',
		'is_admin'        => true,
		'value'           => 1,
		'checked'         => tif_get_option( 'plugin_notification_bar', 'tif_init,enabled', 'checkbox' ),
	),
	$tif_plugin_name . '[tif_init][enabled]'
);

// if ( class_exists ( 'Themes_In_France' ) ) {
	$form->add_input( esc_html__( 'Add to generated files', 'tif-notification-bar' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_init,generated', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_notification_bar', 'tif_init,generated', 'multicheck' ),
			'options'		=> array(
				'css'			=> esc_html__( 'CSS', 'tif-notification-bar' ),
				'js'			=> esc_html__( 'JS', 'tif-notification-bar' ),
			),
		),
		$tif_plugin_name . '[tif_init][generated]'
	);
// }

$form->add_input( esc_html__( 'CSS enabled', 'tif-notification-bar' ),
	array(
		'type'            => 'radio',
		'is_admin'        => true,
		'checked'         => tif_get_option( 'plugin_notification_bar', 'tif_init,css_enabled', 'radio' ),
		'options'         => array(
			''                => esc_html__( 'Plugin CSS (including custom css)', 'tif-notification-bar' ),
			'custom'          => esc_html__( 'Custom CSS only', 'tif-notification-bar' ),
		),
	),
	$tif_plugin_name . '[tif_init][css_enabled]'
);

$form->add_input( esc_html__( 'Allowed roles', 'tif-notification-bar' ),
	array(
		'type'            => 'checkbox',
		'is_admin'        => true,
		'value'           => tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ),
		'checked'         => tif_get_default( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ),
		'options'         => tif_get_wp_roles(),
	),
	$tif_plugin_name . '[tif_init][capabilities]'
);

$form->add_input( esc_html__( 'Custom CSS', 'tif-notification-bar' ),
	array(
		'type'			=> 'textarea',
		'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_init,custom_css', 'textarea' ),
	),
	$tif_plugin_name . '[tif_init][custom_css]'
);
