��    ,      |  ;   �      �     �     �     �     �               .     @     D     P  9   _     �  
   �     �  p   �  =   -     k     |     �     �     �     �     �     �  
   �  	   �     �     �     �  !        )     8     A  '   J  7   r  
   �     �     �     �     �  #   �       *   3  o  ^     �     �     �     	  (   	  !   C	     e	     �	     �	     �	  B   �	     �	     �	     
  {   
  B   �
     �
     �
          !     &     D     G     U     ]     i     z     �     �  +   �     �     �     �  3   �  U   ,     �     �     �     �     �  /   �       *   (            !          )      ,       *                                            "       %                                      	   (           #      &             
   $       '                  +           Add to generated files Allowed roles Background color Bottom Boxed max width Button background color Button text color CSS CSS enabled Closing button Congratulations, the data has been successfully recorded. Content Custom CSS Custom CSS only Determines the date after which the message will no longer be displayed. It will remain displayed all day above. Determines the date from which the message will be displayed. Display end date Display start date Frédéric Caffin Help Is my message boxed? JS Layout Message My Content My Layout My Settings Need some help ? Overlay Plugin CSS (including custom css) Plugin Enabled Position Settings Simply display a message to your users. Sorry, your nonce did not verify. No data was recorded. Text Color Text of the closing button Tif - Message Top Unauthorized user. Your custom CSS has been generated. https://themesinfrance.fr https://themesinfrance.fr/plugins/message/ Project-Id-Version: Tif - Message 0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/tif-message
PO-Revision-Date: 2022-10-11 10:05+0200
Last-Translator: Frédéric Caffin <fc@xymail.fr>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Domain: tif-message
 Ajouter aux fichiers générés Autorisations Couleur d’arrière-plan Bas Largeur maximum des éléments encadrés Couleur d'arrière plan du bouton Couleur du texte du bouton CSS CSS activé Bouton de fermeture Félicitations, les données ont été enregistrées avec succès. Contenu CSS personnalisé CSS personnalisé seulement Détermine la date à partir de laquelle le message ne sera plus affiché. Il restera affiché toute la journée ci-dessus. Détermine la date à partir de laquelle le message sera affiché. Date de fin d'affichage Date de début d'affichage Frédéric Caffin Aide Mon message est-il encadré ? JS Mise en forme Message Mon Contenu Ma Mise en forme Mes Paramètres Besoin d'aide ? Superposition CSS du plugin (inclut le css personnalisé) Plugin activé Position Paramètres Afficher simplement un message à vos utilisateurs. Désolé, votre nonce n'a pas été vérifié. Aucune donnée n'a été enregistrée. Couleur du texte Texte du bouton de fermeture Tif - Message Haut Utilisateur non autorisé. Votre CSS personnalisé a bien été généré. https://themesinfrance.fr https://themesinfrance.fr/plugins/message/ 