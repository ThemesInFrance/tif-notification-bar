<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Notification Bar setup data
 */

function tif_plugin_notification_bar_setup_data() {

	$boxed_width = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : '1200';

	return $tif_notification_bar_setup_data = array(

		'tif_tabs'                  => 1,

		// Settings
		'tif_init'                  => array(
			'enabled'                   => true,
			'generated'                 => array( null ),
			'css_enabled'               => '',
			'capabilities'              => '',
			'custom_css'                => '',
			'id'                        => '123456789',

			'message'                   => "Please read this message before continuing to browse the site. \n Note: You can create paragraphs or add <a href=\"https://themesinfrance.fr/\" target=\"_blank\" rel=\"noreferrer noopener\">links</a> to your message.",
			'close_btn'                 => 'button',
			'close_btn_txt'             => 'Ok',
			'date_start'                => tif_sanitize_date( date( "Y-m-d" ) ),
			'date_end'                  => tif_sanitize_date( date( "Y-m-d" ) ),
		),

		// Layout
		'tif_layout'                => array(
			'layout'                    => 'fixed_bottom',
			'boxed'                     => 1,
			'boxed_width'               => $boxed_width,
			'bgcolor'                   => '#222222',
			'text_color'                => '#ffffff',
			'close_btn_bgcolor'         => '#5CB85C',
			'close_btn_txt_color'       => '#ffffff',
		),

	);

}
