<?php
/**
 * Plugin Name: Tif - Notification Bar
 * Plugin URI: https://themesinfrance.fr/plugins/message/
 * Description: Simply display a message to your users.
 * Version: 0.1
 * Author: Frédéric Caffin
 * Author URI: https://themesinfrance.fr
 * Text Domain: tif-notification-bar
 * Domain Path: /inc/lang
 *
 * License: GPLv2 or later

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ( at your option ) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * -notification-bar ( => tif-notification-bar )
 * _notification_bar ( => tif_notification_bar )
 * _Notification_Bar ( => Tif_Notification_Bar )
 * _NOTIFICATION_BAR ( => TIF_NOTIFICATION_BAR )
 * Notification Bar
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'TIF_NOTIFICATION_BAR_VERSION', '1.0' );
define( 'TIF_NOTIFICATION_BAR_DIR', plugin_dir_path( __FILE__ ) );
define( 'TIF_NOTIFICATION_BAR_URL', plugins_url( '/', __FILE__ ) );
define( 'TIF_NOTIFICATION_BAR_ADMIN_IMAGES_URL', plugins_url( '/assets/img/admin', __FILE__ ) ) ;

/**
 * register_uninstall_hook( $file, $callback );
 *
 * @link https://codex.wordpress.org/Function_Reference/register_uninstall_hook
 */
register_uninstall_hook( __FILE__, 'tif_notification_bar_uninstall' );
function tif_notification_bar_uninstall() {

	delete_option( 'tif_plugin_notification_bar' );

}

if ( ! defined( 'TIF_COMPONENTS_DIR' ) )
	define( 'TIF_COMPONENTS_DIR', ( get_theme_mod( 'tif_components_version' ) ? get_template_directory() . '/tif/' : plugin_dir_path( __FILE__ ) . 'tif/' ) );

require_once TIF_COMPONENTS_DIR . 'init.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-minify.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-assets.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-posts-lists.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-thumbnail.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-mail.php';

// Notification Bar Setup data
require_once 'inc/tif-setup-data.php';

// Notification Bar functions
require_once 'inc/tif-functions.php';

if( is_admin() ) {

	// Back
	require_once 'inc/admin/tif-settings-form.php';
	require_once 'inc/admin/tif-register.php';

}

/**
 * Notification Bar Class Init
 */
class Tif_Notification_Bar_Init {

	/**
	 * Setup class.
	**/
	function __construct() {

		// Notification Bar Back scripts
		// add_action( 'admin_enqueue_scripts', array( $this, 'tif_notification_bar_admin_scripts' ) );

		// Notification Bar Front scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'tif_notification_bar_public_scripts' ), 110 );

		// Notification Bar Translation
		add_action( 'init', array( $this, 'tif_notification_bar_translation' ) );
		// add_action( 'admin_init', array( $this, 'tif_notification_bar_polylang' ) );

		// ... Notification Bar Assets Generation .....................................
		// Add Notification Bar CSS to generated main CSS if configured
		add_filter( 'tif_main_css', array( $this, 'tif_get_compiled_notification_bar_css' ), 30 );

		// Add Notification Bar JS to generated main JS if configured
		add_filter( 'tif_main_js', array( $this, 'tif_get_notification_bar_js' ), 30 );

		// Build standalone Notification Bar CSS
		add_action( 'after_setup_theme', array( $this, 'tif_generate_css_from_fo' ), 20 );
		add_action( 'customize_save_after', array( $this, 'tif_create_notification_bar_css' ), 20, false );

		// Inline Notification Bar CSS if required
		add_action( 'wp_head', array( $this, 'tif_notification_bar_inline_css' ), 20 );

		if( is_admin() ) {

			// Display admin notices
			add_action( 'admin_notices', array( $this, 'tif_notification_bar_admin_notice' ), 10 );

			// Notification Bar Menu
			add_action( 'admin_menu', array( $this, 'tif_notification_bar_menu' ), 100 );

			// Notification Bar Capability
			add_filter( 'option_page_capability_tif-notification-bar', array( $this, 'tif_notification_bar_option_page_capability' ) );

			if ( ! is_customize_preview() )
				// Notification Bar Register
				add_action( 'admin_init', array( $this, 'tif_notification_bar_register' ) );

		} else {

			// Notification Bar Front render function
			add_action( 'wp_footer', 'tif_render_notification_bar', 40 );

		}

	}

	/**
	 * Display admin notices
	 */
	public function tif_notification_bar_admin_notice() {

		global $tif_tweaks_translation;

		$settings_errors = get_settings_errors( 'message_settings_error', false );

		// DEBUG:
		// tif_print_r($settings_errors);

		// Get the current screen
		$screen = get_current_screen();

		// Return if not plugin settings page
		if (
				   $screen->id !== strtolower( $tif_tweaks_translation ) . '_page_tif-notification-bar-options'
				&& $screen->id !== 'settings_page_tif-notification-bar-options'
			)
			return;

		// Checks if settings updated
		if ( isset( $settings_errors[0]['type'] ) && $settings_errors[0]['type'] == 'updated' ) {

			$this->tif_generate_css_from_bo();
			echo '<div class="notice notice-success"><p>' . esc_html__( 'Your custom CSS has been generated.', 'tif-notification-bar' ) . '</p></div>';

			if( $screen->id !== 'settings_page_tif-notification-bar-options' ) {
				echo '<div class="notice notice-success"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

			}

		} elseif ( isset ( $settings_errors[0]['type'] )) {

			echo '<div class="notice notice-warning"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		}

	}

	/**
	 * Get Notification Bar generated assets path
	 */
	static function tif_plugin_assets_dir() {

		$tif_dir = array();
		$dirs = wp_upload_dir();
		$tif_dir['basedir'] = $dirs['basedir'] . '/plugins/tif-notification-bar';
		$tif_dir['baseurl'] = $dirs['baseurl'] . '/plugins/tif-notification-bar';

		/**
		 * @see https://developer.wordpress.org/reference/functions/wp_mkdir_p/
		 */

		$mkdir = array(
			'',
			'/assets',
			'/assets/css',
			// '/assets/css/blocks',
			'/assets/js',
			// '/assets/img',
			// '/assets/fonts'
		);

		foreach ( $mkdir as $key ) {

			if ( ! is_dir( $tif_dir['basedir'] . $key ) )
				wp_mkdir_p( $tif_dir['basedir'] . $key );

		}

		return $tif_dir;

	}

	/**
	 * Notification Bar admin scripts
	 */
	public function tif_notification_bar_admin_scripts( $hook ) {

		if ( 'settings_page_tif-notification-bar-options' != $hook )
			return;

		// Notification Bar Back CSS
		wp_register_style( 'tif-notification-bar-admin', TIF_NOTIFICATION_BAR_URL . 'assets/css/admin/style' . tif_get_min_suffix() . '.css' );
		wp_enqueue_style( 'tif-notification-bar-admin' );

		// Notification Bar Back JS
		wp_register_script( 'tif-notification-bar-admin', TIF_NOTIFICATION_BAR_URL . 'assets/js/admin/script' . tif_get_min_suffix() . '.js', array( 'jquery' ), time(), true );
		wp_enqueue_script( 'tif-notification-bar-admin' );

	}

	/**
	 * Notification Bar front scripts
	 */
	public function tif_notification_bar_public_scripts() {

		$tif_generated = tif_get_option( 'plugin_notification_bar', 'tif_init,generated', 'multicheck' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		$deps          = get_theme_mod( 'tif_components_version' ) ? array( 'tif-main' ) : false;

		if ( ! $deps || ( $deps && ! in_array( 'css', $tif_generated ) && ! is_customize_preview() ) ) {

			// Get Notification Bar CSS dir
			$tif_dir	 = $this->tif_plugin_assets_dir();

			$filetocheck = $tif_dir['basedir'] . '/assets/css/style.css';
			$version	 = file_exists( $filetocheck ) ? date ( 'YdmHis', filemtime( $filetocheck ) ) : time();

			// Notification Bar Front CSS if not added to TIF Main CSS
			wp_register_style( 'tif-notification-bar', $tif_dir['baseurl'] . '/assets/css/style' . tif_get_min_suffix() . '.css', $deps, $version );
			wp_enqueue_style( 'tif-notification-bar' );

		}

		$notification_bar_init	= tif_get_option( 'plugin_notification_bar', 'tif_init', 'array' );
		$notification_bar_layout = tif_get_option( 'plugin_notification_bar', 'tif_layout', 'array' );
		$localized		= 'tif-scripts';

		if ( ! $deps || ( $deps && ! in_array( 'js', $tif_generated ) ) ) {

			$localized	 = 'tif-notification-bar' ;

			// Notification Bar Front JS if not added to TIF Main JS
			wp_register_script( 'tif-notification-bar', TIF_NOTIFICATION_BAR_URL . 'assets/js/script' . tif_get_min_suffix() . '.js', $deps, time(), true );
			wp_enqueue_script( 'tif-notification-bar' );

		}

		$notification_bar_localized_data = array(
			'init' => array(
				'id' => (int)$notification_bar_init['id'],
				'layout' => tif_sanitize_slug( $notification_bar_layout['layout'] ),
				'boxed' => tif_sanitize_slug( isset( $notification_bar_layout['boxed'] ) && $notification_bar_layout['boxed'] ? 'tif-boxed': null ),
				),
			);

		wp_localize_script(
			$localized,
			'tifMessageInit',
			$notification_bar_localized_data,
			false
		);

	}

	/**
	 * Notification Bar capability
	 */
	public function tif_notification_bar_option_page_capability( $capability ) {

		if ( current_user_can( 'edit_posts' ) )
			return 'edit_posts';

	}

	/**
	 * Notification Bar admin menu
	 */
	public function tif_notification_bar_menu() {

		// @link https://www.base64-image.de/
		// @link http://b64.io/

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ) ) ;

		$unify_menus = get_option( 'tif_plugin_tweaks');
		$unify_menus = isset( $unify_menus['tif_init']['unify_menus'] ) && $unify_menus['tif_init']['unify_menus'] ? true : false ;

		if ( ! empty ( $GLOBALS['admin_page_hooks']['tif-tweaks-options'] ) && $unify_menus ) {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_submenu_page/
			 */
			$hook = add_submenu_page(
				'tif-tweaks-options',                   // $parent_slug
				'Plugin Settings',                      // $page_title
				'Notification',                         // $menu_title
				$capability,                            // $capability
				'tif-notification-bar-options',         // $menu_slug
				'tif_notification_bar_options_page',    // $function
				1                                       // $position
			);

		} else {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_options_page/
			 */
			$hook = add_options_page(
				'Plugin Settings',                      // $page_title
				'Tif - Notification',                   // $menu_title
				$capability,                            // $capability
				'tif-notification-bar-options',         // $menu_slug
				'tif_notification_bar_options_page'	    // $function
			);

		}

	}

	/**
	 * register_setting( $option_group, $option_name, $sanitize_callback );
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_setting
	 */
	public function tif_notification_bar_register() {

		if( ! is_customize_preview() )
			register_setting( 'tif-notification-bar', 'tif_plugin_notification_bar', 'tif_notification_bar_sanitize' );

	}

	/**
	 * Loads Notification Bar translated strings
	 *
	 * @link https://developer.wordpress.org/reference/functions/load_plugin_textdomain/
	 */
	public function tif_notification_bar_translation() {

		load_plugin_textdomain( 'tif-notification-bar', false, basename( dirname( __FILE__ ) ) . '/inc/lang' );

	}

	/**
	 * Enable Notification Bar translation with Polylang
	 *
	 * @link https://polylang.pro/doc/function-reference/
	 */
	// public function tif_notification_bar_polylang() {
	//
	// 	if( ! function_exists( 'pll_register_string' ) )
	// 		return;
	//
	// 	pll_register_string(
	// 		esc_html__( 'My text.', 'tif-notification-bar' ),
	// 		tif_get_option( 'plugin_notification_bar', 'tif_options,text', 'text' ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-notification-bar' )
	// 	);
	//
	// 	pll_register_string(
	// 		esc_html__( 'My textarea.', 'tif-notification-bar' ),
	// 		esc_attr( tif_get_option( 'plugin_notification_bar', 'tif_options,textarea', 'textarea' ) ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-notification-bar' )
	// 	);
	//
	// }

	// ... Notification Bar Assets Generation .........................................

	/**
	 * Get Notification Bar assets added to tif main generated
	 */
	public function tif_get_generated_assets() {

		$tif_generated = tif_get_option( 'plugin_notification_bar', 'tif_init,generated', 'multicheck' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		return $tif_generated;

	}

	/**
	 * Notification Bar inline CSS for customize preview
	 */
	public function tif_notification_bar_inline_css() {

		if ( ! is_customize_preview() )
			return;

		echo '<style type="text/css">';

		$this->tif_compile_notification_bar_css( true );

		echo '</style>';

	}

	/**
	 * Get Notification Bar compiled CSS
	 */
	public function tif_get_compiled_notification_bar_css( $css ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(  ! class_exists ( 'Themes_In_France' )
			|| ! in_array( 'css', $tif_generated ) )
			return $css;

		$css .= $this->tif_compile_notification_bar_css();

		return $css;

	}

	/**
	 * Add Notification Bar CSS to tif-main.css if configured
	 */
	private function tif_compile_notification_bar_css( $echo = false, $custom_css = false ) {

		$tif_css_enabled = tif_get_option( 'plugin_notification_bar', 'tif_init,css_enabled', 'key' );
		$layout          = tif_get_option( 'plugin_notification_bar', 'tif_layout', 'array' );
		$boxed_width     = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : $layout['boxed_width'];

		$plugin_scss_components = array();

		if( class_exists( 'Themes_In_France' ) ) {

			$tif_breakpoints = tif_get_option( 'theme_assets', 'tif_css_breakpoints', 'multicheck' );
			$variables = array(
				'$breakpoints'    => '(
					sm: ' . (int)$tif_breakpoints[0] . 'px,
					md: ' . (int)$tif_breakpoints[1] . 'px,
					lg: ' . (int)$tif_breakpoints[2] . 'px,
					xl: ' . (int)$tif_breakpoints[3] . 'px,
				)',
			);

		} else {

			$variables = array(
				'$breakpoints'    => '(
					sm: 576px,
					md: 992px,
					lg: 1330px,
				)',
				'$utils-components'                     => '(
					_utils-global,
					_utils-spacers,
				)',
			);

		}

		$variables = array_merge(
			$variables,
			array(
				'$scss-components'                      => '(' . implode( ',', $plugin_scss_components ) . ')',

				'$tif-width-primary'                    => tif_get_length_value( tif_sanitize_length ( $boxed_width ) ),
				'$tif-width-wide'                       => '1400px',

				'$has-tif-path'                         => get_theme_mod( 'tif_components_version' ) ? "true" : "false",
				'$tif-notification-bar-boxed-width'              => tif_get_length_value( tif_sanitize_length ( $boxed_width ) ),

				// '$tif-notification-bar-color'                    => tif_sanitize_hexcolor($layout['text_color']),
				// '$tif-notification-bar-bg-color'                 => tif_sanitize_hexcolor($layout['bgcolor']),
				// '$tif-notification-bar-link-color'               => tif_sanitize_hexcolor($layout['text_color']),
				// '$tif-notification-bar-btn-color'                => tif_sanitize_hexcolor($layout['close_btn_txt_color']),
				// '$tif-notification-bar-btn-bg-color'             => tif_sanitize_hexcolor($layout['close_btn_bgcolor']),
			)
		);


		$css = null;

		// Compile Plugin CSS
		if ( $tif_css_enabled != 'custom' )
			$css .= tif_compile_scss(
				array(
					'origin'		=> 'plugin',
					'components'	=> array( '_style' ),
					'variables'		=> $variables,
					'path'			=> TIF_NOTIFICATION_BAR_DIR . 'assets/scss'
				)
			);

		$custom_colors = new Tif_Custom_Colors;
		// $color = $custom_colors->tif_colors();

		$css .= '
		/* Tif Notification Bar Colors */
		' .

		$custom_colors->tif_colors_custom(
			'.tif-notification-bar',
			array(
				'color'		=> $layout['text_color'],
				'bgcolor'	=> $layout['bgcolor'],
				'link'		=> $layout['text_color'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-notification-bar .js.is-btn',
			array(
				'bgcolor'	=> $layout['close_btn_bgcolor'],
				'color'		=> $layout['close_btn_txt_color'],
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-notification-bar .is-btn a',
			array(
				'bgcolor'	=> $layout['close_btn_bgcolor'],
				'color'		=> $layout['close_btn_txt_color'],
			)
		) .

		'';

		if ( $custom_css ) {
			// Add custom css after register backoffice form
			$custom_css = is_bool( $custom_css ) ? null : $custom_css ;
			$css .= strip_tags( $custom_css ) . "\n";

		} else {
			// Add custom css customize_save_after
			$css .= strip_tags( tif_get_option( 'plugin_notification_bar', 'tif_init,custom_css', 'textarea' ) ) . "\n";
		}

		if ( $echo )
			echo $css;

		else
			return $css;

	}

	/**
	 * Create Plugin CSS
	 */
	public function tif_create_notification_bar_css( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-notification-bar' ) );

		// Get plugin CSS dir
		$tif_dir	= $this->tif_plugin_assets_dir();

		// Compile plugin CSS
		$plugin_css = $this->tif_compile_notification_bar_css( false, $custom_css );

		// Create CSS
		tif_create_assets(
			array (
				'content'	=> $plugin_css,
				'type'		=> 'css',
				'path'		=> $tif_dir['basedir'] . '/assets/css/',
				'name'		=> 'style'
			)
		);

	}

	/**
	 * Generate Plugin css after register_setting()
	 */
	private function tif_generate_css_from_bo() {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-notification-bar' ) );

		// Generate theme tif-main.css
		if( get_theme_mod( 'tif_components_version' ) ) {
			tif_create_theme_main_css( true );
			tif_create_theme_main_js( true );
		}

		// Generate plugin CSS
		$custom_css = null != get_option( 'tif_plugin_notification_bar' )['tif_init']['custom_css']
					? get_option( 'tif_plugin_notification_bar' )['tif_init']['custom_css']
					: true;
		$generated	= new Tif_Notification_Bar_Init;
		$generated->tif_create_notification_bar_css( strip_tags( $custom_css ) );

	}

	/**
	 * Force Plugin CSS if requested
	 */
	public function tif_generate_css_from_fo( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$tif_dir	= $this->tif_plugin_assets_dir();

		if ( ( isset( $_POST['tif_generate_css_nonce_field'] )
			&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
			&& current_user_can( 'administrator' ) )
			)
			$this->tif_create_notification_bar_css();

		if ( ! file_exists( $tif_dir['basedir'] . '/assets/css/style.css' )
			|| $custom_css
			)
			$this->tif_create_notification_bar_css( $custom_css );

	}

	/**
	 * Add Notification Bar JS to tif-main.js if configured
	 */
	public function tif_get_notification_bar_js( $js ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(    class_exists ( 'Themes_In_France' )
			&& ! in_array( 'js', $tif_generated )
			)
			return $js;

		// Prevent a notice
		$tif_js = array();
		$tif_js_build = '';

		// Array of css files
		$tif_js[] = TIF_NOTIFICATION_BAR_DIR . 'assets/js/script.js';

		// Loop the css Array
		global $wp_filesystem;
		foreach ( $tif_js as $js_file ) {
			$tif_js_build .= $wp_filesystem->get_contents( $js_file );
		}

		// return the generated css
		$js .= $tif_js_build;

		return $js;

	}


}

return new Tif_Notification_Bar_Init();
